module.exports = {
  title: 'PIA Handbook',
  description: 'A highly classified handbook...',
  themeConfig: {
    logo: '/PIA-Logo.png',
    lastUpdated: 'Last updated',
    repo: 'https://gitlab.com/pinewood-builders/pia/pia-handbook',
    yuu: {
      defaultDarkTheme: true,
      defaultColorTheme: 'red',
		},
    editLinks: true,
    editLinkText: 'Recommend a change',
    nav: [
      {
        text: 'Home',
        link: '/'
      },
      {
        text: 'Protocols',
        items: [
          {
            text: 'got you!',
            link: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
          }
        ]
      },
      {
        text: 'Active Investigation',
        items: [
          {
            text: 'XD-001',
            link: 'pia/active-investigations/XD-001'
          },
          {
            text: 'NCC-1701',
            link: 'pia/active-investigations/NCC-1701'
          },
          {
            text: 'INTEL-3909',
            link: 'pia/active-investigations/INTEL-3909'
          }
        ]
      }
    ],

    plugins: ['@vuepress/active-header-links']
  }
}
